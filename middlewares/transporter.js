const nodemailer = require("nodemailer");

//mail sender details
exports.transporter = nodemailer.createTransport({
  service: "gmail",
  auth:{
    user: process.env.auth_user,
    pass: process.env.auth_pass
  },
  tls: {
    rejectUnauthorized: false
  }
});


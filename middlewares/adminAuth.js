const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
const admin_auth = (req, res, next) => {
  const token = req.cookies.admin_token;
  jwt.verify(token, process.env.SECRET, (err, loginUser) => {
    if (!token) {
      return res.status(401).json({ message: "YOU ARE NOT AUTHORIZED" });
    }
    req.admin = loginUser;

    next();
  });
};

module.exports = admin_auth;

const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
exports.currentUser = (req, res, next) => {
  const token = req.cookies.admin_token;

  jwt.verify(token, process.env.SECRET, (err, loginUser) => {
    if (!token) {
      return res.status(401).json("YOU ARE NOT AUTHORIZE ");
    }
    req.user = loginUser;

    next();
  });
};

exports.User = (req, res, next) => {
  const token = req.cookies.acess_token;

  jwt.verify(token, process.env.SECRET, (err, loginUser) => {
    if (!token) {
      return res.status(401).json("YOU ARE NOT AUTHORIZE ");
    }
    req.user = loginUser;

    next();
  });
};

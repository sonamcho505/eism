const Request = require("../../models/submitRequestModel");
const { default: axios } = require("axios");
const techModel = require("../../models/techModel");
const { transporter } = require("../../middlewares/transporter");


exports.getTechnicianForm = async (req, res) => {
  const technicianUsers = await techModel.find({});
  const incidentRequests = await Request.find({ _id: req.params.id });
  const currentUser = req.user.foundUser || req.user;
  res.render("tech/technician_form", {
    data: currentUser,
    incidentRequests: incidentRequests,
    assigns: technicianUsers,
  });
};

exports.postTechnicianForm = async (req, res) => {
  const { itemList, quantity, completeDate,completeTime, addbar } = req.body;
  try {
    const currentUser = req.user.foundUser || req.user;
    const incidentRequest = await Request.findOne({ _id: req.params.id });
    if (!incidentRequest) {
      throw new Error("Document not found");
    }
    await incidentRequest.updateOne({
      $set: {
        completeDate: completeDate,
        completeTime: completeTime,
        itemList: itemList,
        quantity: quantity,
        // addbar: addbar,
        imageUrl: req.file.filename,
        progress_status: "Completed",
        status: "pending"
      },
    });
    var mailOptions = {
      from: "EMS",
      to: '12190082.gcit@rub.edu.bt',
      subject: "Work Completion Notification",
      html: "Hello Manager, <br><br>The work that you have assigned for " + incidentRequest.name+ " has been completed on " + incidentRequest.completeDate+ 
      ". As an evidence, an image and item list has been submitted. <br><br> Thank you, <br><br>" + currentUser.name+ "<br>" +currentUser.designation+ 
      "<br>Estate Incident and Service Management"
    };

    //sending mail
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log("email" + error);
      } else {
        res.redirect("/technician_form");
      }
    });
    res.redirect("/technicianDashboard");
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

exports.getViewAssignRequest = async (req, res) => {
  // const technicianUsers = await techModel.find({});
  // const requests = await Request.find({ _id: req.params.id });
  // const currentUser = req.user.foundUser || req.user;
  // res.render("tech/viewAssignRequest", {
  //   data: currentUser,
  //   requests: requests,
  //   // assigns: technicianUsers,
  // });

  const technicianUsers = await techModel.find({});
  const incidentRequests = await Request.find({ _id: req.params.id });
  const currentUser = req.user.foundUser || req.user;
  res.render("tech/viewAssignRequest", {
    data: currentUser,
    incidentRequests: incidentRequests,
    assigns: technicianUsers,
  });
};
const Request = require("../../models/submitRequestModel");
const { default: axios } = require("axios");

exports.gettechniciantaskproof = async (req, res) => {
  // try {
  //   const incidentRequests = await Request.find({});
  //   const currentUser = req.user.foundUser || req.user;
  //   console.log("hhhhhhh");
  //   res.render("tech/techniciantaskproof", {
  //     data: currentUser,
  //     incidentRequests: incidentRequests,
  //   });
  // } catch (err) {
  //   console.log(err);
  // }
  try {
    const currentUser = req.user.foundUser || req.user;
    const incidentRequests = await Request.find({_delete:false }).sort({_id:-1});
    var inProgress = 0;
    var done = 0;
    var incidentCount = 0;
    incidentRequests.forEach(function (incidentRequest) {
      if (
        currentUser.name === incidentRequest.assignedTechnician.split("-")[0] &&
        incidentRequest.progress_status === "pending"
      ) {
        inProgress++;
        incidentCount++;
      } else if (
        currentUser.name === incidentRequest.assignedTechnician.split("-")[0] &&
        incidentRequest.progress_status === "Completed"
      ) {
        done++;
        incidentCount++;
      }
    });
    res.render("tech/techniciantaskproof", {
      data: currentUser,
      incidentRequests: incidentRequests,
      incidentCount,
      inProgress,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
  }
};

exports.posttechniciantaskproof = async (req, res) => {
  try {
    const value = req.body.data;
    const incidentRequest = await Request.findOne({ _id: value});
    await incidentRequest.updateOne({ _id: value, status: "Completed" });
    res.status(200).json({
      success: true,
      message: "Value received successfully",
      value: value,
    });
  } catch (err) {
    console.log(err);
  }
};

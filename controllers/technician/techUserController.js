const Request = require("../../models/submitRequestModel");
const { default: axios } = require("axios");

exports.getTechnicianDashboard = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const incidentRequests = await Request.find({_delete: false}).sort({_id:-1});
    var inProgress = 0;
    var done = 0;
    var incidentCount = 0;
    incidentRequests.forEach(function (incidentRequest) {
      if (
        currentUser.name === incidentRequest.assignedTechnician.split("-")[0] &&
        incidentRequest.progress_status === "pending"
      ) {
        inProgress++;
        incidentCount++;
      } else if (
        currentUser.name === incidentRequest.assignedTechnician.split("-")[0] &&
        incidentRequest.progress_status === "Completed"
      ) {
        done++;
        incidentCount++;
      }
    });
    res.render("tech/technicianDashboard", {
      data: currentUser,
      incidentRequests: incidentRequests,
      incidentCount,
      inProgress,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
  }
};

exports.postTechnicianDashboard = async (req, res) => {
  try{
    const id = req.body.delete;
    await Request.findByIdAndUpdate(id, { _delete: true });
    // await Request.updateOne({_id: id},{_delete: true});
    res.redirect("/technicianDashboard");
  }catch(err){
    console.log(err);
  }
};

// app.get('/switchToUser', (req, res) => {
//   // Set a session or cookie to remember the switch to user state
//   req.session.switchToUser = true;
//   res.redirect('/userDashboard');
// });

exports.getSwitchTechnician = async(req, res) => {
  // Set a session or cookie to remember the switch to user state
  res.redirect('/userdashboard');
};
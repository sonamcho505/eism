const Request = require("../../models/submitRequestModel");

exports.getTechnicianServiceRequest = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const serviceRequests = await Request.find({ requestType: "Service", _delete:false }).sort({_id:-1});
    var serviceCount = 0;
    var done = 0;
    var inProgress = 0;
    serviceRequests.forEach((serviceRequest) => {
      if(currentUser.name === serviceRequest.assignedTechnician.split("-")[0]){

      if (serviceRequest.progress_status === "pending") {
        inProgress++;
        serviceCount++;
      } else {
        done++;
        serviceCount++;
      }
    }
    });

    res.render("tech/technician_service_request", {
      data: currentUser,
      serviceRequests: serviceRequests,
      serviceCount,
      inProgress,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
  }
};

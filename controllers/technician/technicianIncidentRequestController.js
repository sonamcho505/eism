const Request = require("../../models/submitRequestModel");

exports.getTechnicianIncidentRequest = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const incidentRequests = await Request.find({ requestType: "Incident", _delete:false }).sort({_id:-1});
    var incidentCount = 0;

    var inProgress = 0;
    var done = 0;
    incidentRequests.forEach((incidentRequest) => {
      if(currentUser.name === incidentRequest.assignedTechnician.split("-")[0]){
        if (incidentRequest.progress_status === "pending") {
          inProgress++;
          incidentCount++;
        } else {
          done++;
          incidentCount++;
        }
      }
    });
    res.render("tech/technician_incident_request", {
      data: currentUser,
      incidentRequests: incidentRequests,
      incidentCount,
      inProgress,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
  }
};

exports.postTechnicianIncidentRequest = async (req, res) => {};

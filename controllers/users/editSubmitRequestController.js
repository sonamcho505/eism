const Request = require("../../models/submitRequestModel");
exports.geteditSubmitRequest = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const _id = req.params.id;
    const edit = req.query.edit

    
    const foundRequest = await Request.findById(_id);
    if (!foundRequest) {
      return res.status(404).send("Request not found");
    }
    
    res.render("users/editSubmitRequest", {
      data: currentUser,
      requests: foundRequest,
      edit
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
};


exports.posteditSubmitRequest = async (req, res) => {
  const _id = req.params.id;
  
  try {
    const {
      roomNo,
      memberType,
      block,
      houseNo,
      requestType,
      requestDate,
      requestTime,
      description,
    } = req.body;

    const foundRequest = await Request.findById({ _id });

    if (!foundRequest) {
      return res.status(404).send({ error: "staff not found" });
    }

    await Request.updateOne(
      { _id: foundRequest._id },
      {
        $set: {
          roomNo,
          memberType,
          block,
          houseNo,
          requestType,
          requestDate,
          requestTime,
          description,
        },
      }
    );
    console.log("Request updated successfully");
    res.redirect("/userdashboard");
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Internal server error" });
  }
};
// view specific request of users
exports.getviewRequest = async (req, res) => {
  // const technicianUsers = await techModel.find({});
  const requests = await Request.find({ _id: req.params.id });
  const currentUser = req.user.foundUser || req.user;
  console.log(currentUser)
  res.render("users/viewRequest", {
    data: currentUser,
    requests: requests,
    // assigns: technicianUsers,
  });
};

const Request = require("../../models/submitRequestModel");

exports.getWorkStatus = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    console.log(currentUser.name);
    const requests = await Request.find({ name: currentUser.name, soft_delete: false }).sort({_id:-1});
    const requestCount = requests.length;
    res.render("users/workstatus", {
      data: currentUser,
      requests: requests,
      requestCount,
    });
    // console.log(requests);
  } catch (error) {
    console.log("Error getting requests:", error);
    res.render("users/workstatus", {
      data: currentUser,
      requests: [],
      requestCount: 0,
    });
  }
};



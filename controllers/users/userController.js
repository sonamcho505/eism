const { default: axios } = require("axios");
const jwt = require("jsonwebtoken");
const Request = require("../../models/submitRequestModel");
const techModel = require("../../models/techModel");
const bcrypt = require("bcrypt");
const { transporter } = require("../../middlewares/transporter");
const crypto = require('crypto');

const dotenv = require("dotenv");
const {
  username,
  Password,
  AUTH,
  STD_DETAILS,
} = require("../../api/webServices");
const { ExplainVerbosity } = require("mongodb");
dotenv.config();

exports.loadLogin = async (req, res) => {
  res.render("users/login");
};

exports.viewUserDashboard = async (req, res, next) => {
  try {
    const currentUser = req.user.foundUser || req.user;

    const requests = await Request.find({ name: currentUser.name, soft_delete: false }).sort({_id:-1});
    const requestCount = requests.length;
    let pending = 0;
    let done = 0;
    let inProgress = 0;

    requests.forEach((request) => {
      if (request.assignedTechnician !== "") {
        if (request.progress_status === "Completed") {
          done++;
        } else {
          inProgress++;
        }
      } else {
        if (request.progress_status === "pending") {
          pending++;
        } else {
          done++;
        }
      }
    });
    res.render("users/userdashboard", {
      data: currentUser,
      requests,
      requestCount,
      pending,
      done,
      inProgress,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
    next(error);
  }
};

exports.getLogout = (req, res) => {
  res.cookie("access_token", "", {
    maxAge: 0,
  });
  res.render("users/login");
};
// exports.postUserLogin = async (req, res) => {
//   try {
//     const { email, sid } = req.body;
//     const foundUser = await techModel.findOne({ email: email });
//     const getToken = await axios
//     .post(AUTH, {
//         username: username,
//         password: Password,
//       })
//       .then(async (response) => {
//         const token = response.data.token;
//         const admin_details = response.data.data.user.rows;
//         admin_details.map(async (data) => {
//           if (
//             data.name === email &&
//             (await bcrypt.compare(sid, data.password))
//           ) {
//             const token = jwt.sign(data, process.env.SECRET);
//             res.cookie("admin_token", token, { httpOnly: true });
//             if (data.roleid === 3) {
//               return res.redirect("/managerDashboard");
//             }
//             return res.status(200).send(`successfully logged in : ${token}`);
//           } else if (foundUser) {
//             const token = jwt.sign({ foundUser }, process.env.SECRET);
//             res.cookie("acess_token", token, { httpOnly: true });
//             if (foundUser.role === "user") {
//               return res.redirect("/userdashboard");
//             } else if (foundUser.role === "technician") {
//               return res.redirect("/technicianDashboard");
//             }
//             return res.redirect("/managerDashboard");
//           } else {
//             await axios
//               .get(STD_DETAILS, {
//                 headers: {
//                   Authorization: `Bearer ${token}`,
//                 },
//               })
//               .then(async (response) => {
//                 const studentInfo = response.data;
//                 console.log(typeof studentInfo);
//                 const user = await studentInfo.find(
//                   (detail) =>
//                     detail.email === email && detail.sid === parseInt(sid)
//                 );
//                 if (user) {
//                   const token = jwt.sign(user, process.env.SECRET);
//                   res.cookie("acess_token", token, { httpOnly: true });
//                   return res.redirect("/userdashboard");
//                 }
//                 console.log(email + sid);
//                 res.render('users/login',{message:"Invalid login, please try again."});
//                 // return res.send("invalid password or id");
//               })
//               .catch((error) => {
//                 console.error(
//                   "Failed to access restricted API endpoint:",
//                   error
//                 );
//               });
//           }
//         });
//       })
//       .catch((error) => {
//         console.error("Failed to authenticate with API:", error);
//       });
//   } catch (e) {
//     console.log(e);
//   }
// };


//-------email and password login---------
exports.postUserLogin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const foundUser = await techModel.findOne({ email: email, password: password });

    if (foundUser) {
      const token = jwt.sign({ foundUser }, process.env.SECRET);
      res.cookie("acess_token", token, { httpOnly: true });

      if (foundUser.role === "user") {
        return res.redirect("/userdashboard");
      } else if (foundUser.role === "technician") {
        return res.redirect("/technicianDashboard");
      } else {
        return res.redirect("/managerDashboard");
      }
    }

    const getToken = await axios
      .post(AUTH, {
        username: username,
        password: Password,
      })
      .then(async (response) => {
        const token = response.data.token;
        const admin_details = response.data.data.user.rows;
        for (const data of admin_details) {
          if (data.name === email && (await bcrypt.compare(password, data.password))) {
            const token = jwt.sign(data, process.env.SECRET);
            res.cookie("admin_token", token, { httpOnly: true });
            if (data.roleid === 3) {
              return res.redirect("/managerDashboard");
            }
            return res.status(200).send(`Successfully logged in: ${token}`);
          }
        }

        const studentResponse = await axios.get(STD_DETAILS, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        const studentInfo = studentResponse.data;
        console.log(typeof studentInfo);
        for (const detail of studentInfo) {
          if (detail.email === email && bcrypt.compareSync(password, detail.password)) {
            const token = jwt.sign(detail, process.env.SECRET);
            res.cookie("acess_token", token, { httpOnly: true });
            return res.redirect("/userdashboard");
          }
        }

        res.render('users/login', { message: "Invalid login, please try again." });
      })
      .catch((error) => {
        console.error("Failed to authenticate with API:", error);
      });
  } catch (e) {
    console.log(e);
  }
};

// exports.postUserLogin = async (req, res) => {
//   try {
//     const { email, password } = req.body;
//     const foundUser = await techModel.findOne({ email: email, password: password });
//     const getToken = await axios
//       .post(AUTH, {
//         username: username,
//         password: Password,
//       })
//       .then(async (response) => {
//         const token = response.data.token;
//         const admin_details = response.data.data.user.rows;
//         admin_details.map(async (data) => {
//           if (
//             data.name === email &&
//             (await bcrypt.compare(password, data.password))
//           ) {
//             const token = jwt.sign(data, process.env.SECRET);
//             res.cookie("admin_token", token, { httpOnly: true });
//             if (data.roleid === 3) {
//               return res.redirect("/managerDashboard");
//             }
//             return res.status(200).send(`successfully logged in : ${token}`);
//           } else if (foundUser) {
//             const token = jwt.sign({ foundUser }, process.env.SECRET);
//             res.cookie("acess_token", token, { httpOnly: true });
//             if (foundUser.role === "user") {
//               return res.redirect("/userdashboard");
//             } else if (foundUser.role === "technician") {
//               return res.redirect("/technicianDashboard");
//             }
//             return res.redirect("/managerDashboard");
//           } else {
//             await axios
//               .get(STD_DETAILS, {
//                 headers: {
//                   Authorization: `Bearer ${token}`,
//                 },
//               })
//               .then(async (response) => {
//                 const studentInfo = response.data;
//                 console.log(typeof studentInfo);
//                 const user = await studentInfo.map(
//                    async detail =>{
//                     if(detail.email === email && await bcrypt.compare(password, detail.password)){

//                       const token = jwt.sign(detail, process.env.SECRET);
//                       res.cookie("acess_token", token, { httpOnly: true });
//                       return res.redirect("/userdashboard");
//                     }
//                     else{
//                     //   console.log(email + password);
//                     // res.render('users/login',{message:"Invalid login, please try again."});
//                     }
//                    });
//               //  console.log( await user)               
//               })
//               .catch((error) => {
//                 console.error(
//                   "Failed to access restricted API endpoint:",
//                   error
//                 );
//               });
//           }
//         });
//       })
//       .catch((error) => {
//         console.error("Failed to authenticate with API:", error);
//       });
//   } catch (e) {
//     console.log(e);
//   }
// };

exports.postDelete = async(req, res) => {
  try{
    const id = req.body.delete;
    await Request.findByIdAndDelete(id);
    res.redirect("/userdashboard");
  }catch(err){
    console.log(err);
  }
}

// app.get('/forgot-password', (req, res) => {
//   res.render('forgot-password');
// });
exports.getForgotPassword = (req, res) => {
  res.render('users/forgot-password');
};
function generateUniqueToken() {
  return crypto.randomBytes(20).toString('hex');
}
// Handle POST request to initiate password reset
// app.post('/forgot-password', async (req, res) => {
  exports.postForgotPassword = async (req, res) => {
    try {
      const { email } = req.body;
  
      // Check if the user exists in the database
      const user = await techModel.findOne({ email });
      if (!user) {
        return res.render('users/forgot-password', { errorMessage: 'Invalid email address' });
        // or res.redirect('/error-page') to redirect to an error page
      }
  
      // Generate a unique token for the password reset link
      const token = generateUniqueToken();
  
      // Store the token and its associated user in the database
      user.resetToken = token;
      await user.save();
  
      // Send the password reset email to the user's email address
      const baseUrl = process.env.NODE_ENV === 'production' ? 'https://eism.onrender.com' : 'http://localhost:8000';
      const resetLink = `${baseUrl}/reset-password?token=${token}`;

      // const resetLink = `http://localhost:8000/reset-password?token=${token}`;
      const mailOptions = {
        from: '12190082.gcit@rub.edu.bt',
        to: email,
        subject: 'Password Reset',
        text: `Click the link below to reset your password:\n\n${resetLink}`,
      };
      await transporter.sendMail(mailOptions);
  
      res.redirect('/forgot-password-success'); // Redirect to a success page
    } catch (error) {
      console.error(error);
      res.redirect('/forgot-password'); // Redirect to the same page on error
    }
  };
  

// Password Reset page
// app.get('/reset-password', (req, res) => {
//   res.render('reset-password');
// });
exports.getResetPassword = (req, res) => {
  const token = req.query.token; // Assuming the token is passed as a query parameter
  res.render('users/reset-password',{ token });
};

// Handle POST request to reset the password
// app.post('/reset-password', async (req, res) => {
  exports.postResetPassword = async (req, res) => {

  try {
    const { token, password, confirmPassword } = req.body;

    // Find the user with the provided token
    const user = await techModel.findOne({ resetToken: token });
    if (!user) {
      return res.redirect('/reset-password'); // Redirect to the same page if the token is invalid or expired
    }

    // Update the user's password
    user.password = password;
    user.resetToken = null;
    await user.save();

    res.render('users/login', {message: 'Successfully Change the password.'}); // Redirect to the login page
  } catch (error) {
    console.error(error);
    res.redirect('/reset-password'); // Redirect to the same page on error
  }
};

// app.get('/forgot-password-success', (req, res) => {
exports.getForgotPasswordSuccess = (req, res) => {

  res.render('users/forgot-password-success'); // Render the forgot-password-success view
};

exports.getResetPasswordSuccess = (req, res) => {

  res.render('users/reset-password-success'); // Render the forgot-password-success view
};
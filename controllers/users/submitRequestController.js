const { transporter } = require("../../middlewares/transporter");
const Request = require("../../models/submitRequestModel");
const express = require("express");

exports.getSubmitRequest = async (req, res) => {
  const currentUser = req.user.foundUser || req.user;
  res.render("users/submitrequest", {
    data: currentUser,
  });
};

exports.postSubmitRequest = async (req, res) => {
  try {
    const {
      name,
      email,
      contact,
      roomNo,
      houseNo,
      memberType,
      block,
      requestType,
      requestDate,
      requestTime,
      description,
    } = req.body;
    const request = new Request({
      name: name,
      email: email,
      contactNo: contact,
      memberType: memberType,
      block: block,
      roomNo: roomNo,
      houseNo: houseNo,
      requestType: requestType,
      requestDate: requestDate,
      requestTime: requestTime,
      description: description,
      assignedTechnician: "",
      itemList: "",
      quantity: "",
      imageUrl: "",
      status: "pending",
      progress_status: "pending",
      remarks: ""
    });
    await request.save();

    var mailOptions = {
      from: "Estate Management",
      to: "12190082.gcit@rub.edu.bt",
      subject: "New Request Notification",
      html: "Dear EISM manager, <br> You have received a request from " + name+ " on " + requestDate+ 
      ". Please review the details below: <br><br> Request type: " + requestType+ " <br> Description: " + description+ 
      "<br><br> To access the request and begin working on it, please log in to the system using your credentials. <br><br> Best regards, <br><br> Estate Incident and Service Management" 
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(err);
      } else {
        console.log("Request saved successfully");
        res.redirect("/userdashboard");
      }
    });
  } catch (error) {
    console.log("Error saving request:", error);
    res.redirect("/userdashboard");
  }
};

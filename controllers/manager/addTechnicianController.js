const techModel = require("../../models/techModel");
const multer = require('multer');
const fs = require('fs');
const csv = require('csv-parser');
const flash = require('connect-flash');
const session = require('express-session');

// Define storage for uploaded files


exports.getTechUser = async (req, res) => {
  const currentUser = req.user.foundUser || req.user;

  res.render("manager/add_technician", {
    data: currentUser,
  });
};

exports.getUser = async (req, res) => {
  const currentUser = req.user.foundUser || req.user;
  res.render("manager/add_user", {
    data: currentUser,
  });
};

// exports.postTechUser = async (req, res) => {
//   const { employee_id, name, email, contact, role, designation, password } =
//     req.body;

//   // Check if all required fields are present in the request body
//   if (
//     !employee_id ||
//     !name ||
//     !email ||
//     !contact ||
//     !role ||
//     !designation ||
//     !password
//   ) {
//     return res.status(400).send({ error: "Missing required field(s)" });
//   }

//   try {
//     // Check if a technician with the same employee_id already exists
//     const existingTechnician = await techModel.findOne({ employee_id });
//     const existingEmail = await techModel.findOne({ email });
//     const existingPhone = await techModel.findOne({ contact });

//     if (existingTechnician || existingEmail || existingPhone) {
//       return res.status(400).send({ error: "This Employee_id or Email or Contact no. is already registered" });
//       // return res.render('manager/add_technician',{ error: "This Employee_id or Email or Contact no. is already registered" });

//     }

//     // Create a new technician object and save it to the database
//     const newTechnician = new techModel({
//       employee_id,
//       name,
//       email,
//       contact,
//       role,
//       designation,
//       password,
//     });
//     await newTechnician.save();

//     res.redirect("/technician_details");
//   } catch (err) {
//     console.error(err);
//     res.status(500).send({ error: "Failed to create technician" });
//   }
// };
exports.postTechUser = async (req, res) => {
  const { employee_id, name, email, contact, role, designation, password } = req.body;

  // Check if all required fields are present in the request body
  if (!employee_id || !name || !email || !contact || !role || !designation || !password) {
    return res.send('<script>alert("Missing required field(s)"); window.location.href = "/add_technician";</script>');
  }

  try {
    // Check if a technician with the same employee_id, email, or contact already exists
    const existingTechnician = await techModel.findOne({ employee_id });
    const existingEmail = await techModel.findOne({ email });
    const existingPhone = await techModel.findOne({ contact });

    if (existingTechnician) {
      return res.send('<script>alert("This Employee ID is already registered"); window.location.href = "/add_technician";</script>');
    }

    if (existingEmail) {
      return res.send('<script>alert("This Email is already registered"); window.location.href = "/add_technician";</script>');
    }

    if (existingPhone) {
      return res.send('<script>alert("This Contact no. is already registered"); window.location.href = "/add_technician";</script>');
    }

    // Create a new technician object and save it to the database
    const newTechnician = new techModel({ employee_id, name, email, contact, role, designation, password });
    await newTechnician.save();

    return res.send('<script>alert("Technician registered successfully"); window.location.href = "/technician_details";</script>');
  } catch (err) {
    console.error(err);
    return res.send('<script>alert("Failed to register technician"); window.location.href = "/add_technician";</script>');
  }
};

exports.postUser = async (req, res) => {
  let { employee_id, name, email, contact, role, password } = req.body;

  // Check if all required fields are present in the request body
  if (!employee_id || !name || !email || !contact || !role || !password) {
    return res.status(400).send({ error: "Missing required field(s)" });
  }

  try {
    // Check if a technician with the same employee_id already exists
    const existingTechnician = await techModel.findOne({ employee_id });
    const existingEmail = await techModel.findOne({ email });
    const existingPhone = await techModel.findOne({ contact });

    if (existingTechnician) {
      return res.send('<script>alert("This Employee ID is already registered"); window.location.href = "/add_user";</script>');
    }

    if (existingEmail) {
      return res.send('<script>alert("This Email is already registered"); window.location.href = "/add_user";</script>');
    }

    if (existingPhone) {
      return res.send('<script>alert("This Contact no. is already registered"); window.location.href = "/add_user";</script>');
    }


    // Create a new technician object and save it to the database
    const newTechnician = new techModel({
      employee_id,
      name,
      email,
      contact,
      role,
      password,
    });
    await newTechnician.save();
    return res.send('<script>alert("Staff registered successfully"); window.location.href = "/staff_details";</script>');
  } catch (err) {
    console.error(err);
    return res.send('<script>alert("Failed to register Staff"); window.location.href = "/staff_details";</script>');
  }
   
};

exports.getTechnicianUpdateForm = async (req, res) => {
  try {
    const _id = req.params.id;
    const foundTechUser = await techModel.findById({ _id });
    const currentUser = req.user.foundUser || req.user;
    res.render("manager/technician_update_form", {
      data: currentUser,
      foundTechUser,
    });
  } catch (err) {
    console.log(err);
  }
};
exports.postTechnicianUpdateForm = async (req, res) => {
  try {
    const { employee_id, name, email, contact, role, designation, password } =
      req.body;

    // Check if all required fields are present in the request body
    if (
      !employee_id ||
      !name ||
      !email ||
      !contact ||
      !role ||
      !designation ||
      !password
    ) {
      return res.status(400).send({ error: "Missing required field(s)" });
    }

    const techUser = await techModel.findById(req.params.id);

    if (!techUser) {
      return res.status(404).send({ error: "Technician not found" });
    }

    await techModel.updateOne(
      { _id: techUser._id },
      {
        $set: {
          employee_id,
          name,
          email,
          contact,
          role,
          designation,
          password,
        },
      }
    );

    console.log("Technician updated successfully");
    res.redirect("/technician_details");
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Internal server error" });
  }
};


exports.getUserUpdateForm = async (req, res) => {
  try {
    const _id = req.params.id;
    const foundTechUser = await techModel.findById({ _id });
    const currentUser = req.user.foundUser || req.user;
    res.render("manager/staff_update_form", {
      data: currentUser,
      foundTechUser,
    });
  } catch (err) {
    console.log(err);
  }
};
exports.postUserUpdateForm = async (req, res) => {
  try {
    const { employee_id, name, email, contact, role, password } =
      req.body;

    // Check if all required fields are present in the request body
    if (
      !employee_id ||
      !name ||
      !email ||
      !contact ||
      !role ||
      !password
    ) {
      return res.status(400).send({ error: "Missing required field(s)" });
    }

    const techUser = await techModel.findById(req.params.id);

    if (!techUser) {
      return res.status(404).send({ error: "staff not found" });
    }

    await techModel.updateOne(
      { _id: techUser._id },
      {
        $set: {
          employee_id,
          name,
          email,
          contact,
          role,
          password,
        },
      }
    );

    console.log("Staff updated successfully");
    res.redirect("/staff_details");
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: "Internal server error" });
  }
};

// exports.postBulkRegister('/bulk-registration', upload.single('csvFile'), (req, res) => {
  exports.postTechnicianBulkRegister = async(req, res) => {

  if (!req.file) {
    return res.status(400).send({ error: 'No file uploaded' });
  }

  const filePath = req.file.path;

  fs.createReadStream(filePath)
    .pipe(csv())
    .on('data', async (row) => {
      const technician = new techModel({
        employee_id: row.employee_id,
        name: row.name,
        email: row.email,
        designation: row.designation,
        role: 'technician',
        contact: row.contact,
        password: row.password,

      });

      await technician.save();
    })
    .on('end', () => {
      fs.unlinkSync(filePath);
      res.redirect('/technician_details');
    })
    .on('error', (err) => {
      console.error(err);
      res.status(500).send({ error: 'Failed to process CSV file' });
    });
};

// Download CSV Template endpoint
// exports.getBulkRegister('/download-csv-template', (req, res) => {

exports.getTechnicianBulkRegister = async (req, res) => {
  const csvTemplate = `employee_id,name,email,designation,contact,password`;

  res.set('Content-Disposition', 'attachment; filename=technician_template.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csvTemplate);
};


// Technician Details page endpoint
// app.get('/technician_details', async (req, res) => {
//   try {
//     const technicians = await techModel.find();
//     res.render('technician_details', { technicians });
//   } catch (err) {
//     console.error(err);
//     res.status(500).send({ error: 'Failed to fetch technician details' });
//   }
// });

exports.postUserBulkRegister = async(req, res) => {

  if (!req.file) {
    return res.status(400).send({ error: 'No file uploaded' });
  }

  const filePath = req.file.path;

  fs.createReadStream(filePath)
    .pipe(csv())
    .on('data', async (row) => {
      const technician = new techModel({
        employee_id: row.employee_id,
        name: row.name,
        email: row.email,
        // designation: row.designation,
        role: 'user',
        contact: row.contact,
        password: row.password,

      });

      await technician.save();
    })
    .on('end', () => {
      fs.unlinkSync(filePath);
      res.redirect('/staff_details');
    })
    .on('error', (err) => {
      console.error(err);
      res.status(500).send({ error: 'Failed to process CSV file' });
    });
};

// Download CSV Template endpoint
// exports.getBulkRegister('/download-csv-template', (req, res) => {

exports.getUserBulkRegister = async (req, res) => {
  const csvTemplate = `employee_id,name,email,contact,password`;

  res.set('Content-Disposition', 'attachment; filename=staff_template.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(csvTemplate);
};

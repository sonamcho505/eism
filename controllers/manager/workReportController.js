const Request = require("../../models/submitRequestModel");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;

const fs = require("fs");

exports.getWorkReport = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const requests = await Request.find({}).sort({ _id: -1 });
    const requestCount = requests.length;
    let pending = 0;
    let done = 0;
    let inProgress = 0;
    requests.forEach((request) => {
      if (request.assignedTechnician !== "") {
        if (
          request.progress_status === "Completed" &&
          request.status === "Completed"
        ) {
          done++;
        } else {
          inProgress++;
        }
      } else {
        if (
          request.progress_status === "pending" &&
          request.status === "pending"
        ) {
          pending++;
        } else {
          done++;
        }
      }
    });
    res.render("manager/work_report", {
      data: currentUser,
      requests: requests,
      requestCount,
      inProgress,
      pending,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
    res.render("manager/work_report", {
      data: currentUser,
      requests: [],
      requestCount: 0,
    });
  }
};

exports.getDownloadCSVFile = async (req, res) => {
  const { startDate, endDate } = req.query;
  const requests = await Request.find({
    requestDate: {
      $gte: startDate,
      $lte: endDate,
    },
  });

  const csvWriter = createCsvWriter({
    path: `requests_${startDate}_to_${endDate}.csv`,
    header: [
      { id: "name", title: "Name" },
      { id: "email", title: "Email" },
      { id: "contactNo", title: "Contact Number" },
      { id: "memberType", title: "Member Type" },
      { id: "block", title: "Block" },
      { id: "roomNo", title: "Room Number" },
      { id: "requestType", title: "Request Type" },
      { id: "requestDate", title: "Request Date" },
      { id: "requestTime", title: "Request Time" },
      { id: "description", title: "Description" },
      { id: "assignedTechnician", title: "Assigned Technician" },
      { id: "status", title: "Status" },
      { id: "completeDate", title: "Complete Date" },
    ],
  });

  res.setHeader(
    "Content-Disposition",
    `attachment; filename=requests_${startDate}_to_${endDate}.csv`
  );
  res.setHeader("Content-Type", "text/csv");

  csvWriter.writeRecords(requests).then(() => {
    const readStream = fs.createReadStream(
      `requests_${startDate}_to_${endDate}.csv`
    );
    readStream.pipe(res);
  });
};

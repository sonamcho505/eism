const Request = require("../../models/submitRequestModel");

exports.getServiceRequest = async (req, res) => {
  // try {
  //   const currentUser = req.user.foundUser || req.user;
  //   const serviceRequests = await Request.find({ requestType: "Service" });
  //   const serviceCount = serviceRequests.length;
  //   var done = 0;
  //   var pending = 0;
  //   serviceRequests.forEach((serviceRequest) => {
  //     if (serviceRequest.progress_status === "pending") {
  //       pending++;
  //     } else {
  //       done++;
  //     }
  //   });

  //   res.render("manager/service_request", {
  //     data: currentUser,
  //     serviceRequests: serviceRequests,
  //     serviceCount,
  //     pending,
  //     done,
  //   });
  // } catch (error) {
  //   console.log("Error getting requests:", error);
  // }

  try {
    const currentUser = req.user.foundUser || req.user;
    const requests = await Request.find({});
    const serviceRequests = await Request.find({ requestType: "Service" }).sort({_id:-1});
    const serviceCount = serviceRequests.length;    
    let pending = 0;
    let done = 0;
    let inProgress = 0;
    serviceRequests.forEach(serviceRequest => {
      if(serviceRequest.assignedTechnician !== ""){
        if (serviceRequest.progress_status === "Completed" && serviceRequest.status === "Completed") {
          done++;
        }else{
          inProgress++;
        }
      }else{
        if (serviceRequest.progress_status === "pending" && serviceRequest.status === "pending") {
          pending++;
        } else {
          done++;
        }
      }
    });
    res.render("manager/service_request", {
      data: currentUser,
      // requests: requests,
      // requestCount,
      serviceRequests: serviceRequests,
      serviceCount,
      inProgress,
      pending,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
    res.render("manager/service_request", {
      data: currentUser,
      serviceRequests: [],
      serviceCount: 0,
    });
  }
};

const Request = require("../../models/submitRequestModel");
const { transporter } = require("../../middlewares/transporter");
const { default: axios } = require("axios");
const techModel = require("../../models/techModel");
const bcrypt = require("bcrypt");
const crypto = require('crypto');
const dotenv = require("dotenv");

exports.getManagerDashboard = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;
    const requests = await Request.find({}).sort({_id:-1});
    const requestCount = requests.length;
    let pending = 0;
    let done = 0;
    let inProgress = 0;
    requests.forEach(request => {
      if(request.assignedTechnician !== ""){
        if (request.progress_status === "Completed" && request.status === "Completed") {
          done++;
        }else{
          inProgress++;
        }
      }else{
        if (request.progress_status === "pending" && request.status === "pending") {
          pending++;
        } else {
          done++;
        }
      }
    });
    res.render("manager/managerDashboard", {
      data: currentUser,
      requests: requests,
      requestCount,
      inProgress,
      pending,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
    res.render("manager/managerDashboard", {
      data: currentUser,
      requests: [],
      requestCount: 0,
    });
  }
};
exports.getUserViewRequest = async (req, res) => {
  // const technicianUsers = await techModel.find({});
  const requests = await Request.find({ _id: req.params.id });
  const currentUser = req.user.foundUser || req.user;
  res.render("manager/viewUserRequest", {
    data: currentUser,
    requests: requests,
    // assigns: technicianUsers,
  });
};

exports.getSwitchManager = async (req, res) => {
  const currentUser = req.user.foundUser || req.user;

  // Store the currentUser data in the session
  req.session.currentUser = currentUser;

  // Redirect to "/userdashboard" route
  res.redirect("/userdashboard");
};



exports.postManagerDashboard = async (req, res) => {};

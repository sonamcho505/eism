const Request = require("../../models/submitRequestModel");

exports.getIncidentRequest = async (req, res) => {
  // try {
  //   const currentUser = req.user.foundUser || req.user;
  //   const incidentRequests = await Request.find({ requestType: "Incident" });
  //   const incidentCount = incidentRequests.length;

  //   var pending = 0;
  //   var done = 0;
  //   incidentRequests.forEach((incidentRequest) => {
  //     if (incidentRequest.progress_status === "pending") {
  //       pending++;
  //     } else {
  //       done++;
  //     }
  //   });
  //   res.render("manager/incident_request", {
  //     data: currentUser,
  //     incidentRequests: incidentRequests,
  //     incidentCount,
  //     pending,
  //     done,
  //   });
  // } catch (error) {
  //   console.log("Error getting requests:", error);
  // }
  try {
    const currentUser = req.user.foundUser || req.user;
    const incidentRequests = await Request.find({ requestType: "Incident" }).sort({_id:-1});
    const incidentCount = incidentRequests.length;
    let pending = 0;
    let done = 0;
    let inProgress = 0;
    incidentRequests.forEach(incidentRequest => {
      if(incidentRequest.assignedTechnician !== ""){
        if (incidentRequest.progress_status === "Completed" && incidentRequest.status === "Completed") {
          done++;
        }else{
          inProgress++;
        }
      }else{
        if (incidentRequest.progress_status === "pending" && incidentRequest.status === "pending") {
          pending++;
        } else {
          done++;
        }
      }
    });
    res.render("manager/incident_request", {
      data: currentUser,
      // requests: requests,
      // requestCount,
      incidentRequests: incidentRequests,
      incidentCount,
      inProgress,
      pending,
      done,
    });
  } catch (error) {
    console.log("Error getting requests:", error);
    res.render("manager/incident_request", {
      data: currentUser,
      incidentRequests: [],
      incidentCount: 0,
    });
  }
};

exports.postIncidentRequest = async (req, res) => {};

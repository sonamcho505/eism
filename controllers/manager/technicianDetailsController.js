const techModel = require("../../models/techModel");
const Request = require("../../models/submitRequestModel");
exports.getTechnicianDetail = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;

    // Retrieve all technician documents from the database
    const technicians = await techModel.find({ role: "technician" });

    // Render the retrieved technician documents in your view engine (EJS)
    res.render("manager/technician_details", {
      data: currentUser,
      technicians: technicians,
    });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .send("Failed to retrieve technician details from the database.");
  }
};

exports.getUserDetail = async (req, res) => {
  try {
    const currentUser = req.user.foundUser || req.user;

    // Retrieve all technician documents from the database
    const technicians = await techModel.find({ role: "user" });
    console.log(technicians);

    // Render the retrieved technician documents in your view engine (EJS)
    res.render("manager/staff_details", {
      data: currentUser,
      technicians: technicians,
    });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .send("Failed to retrieve technician details from the database.");
  }
};

exports.postUserDetail = async (req, res) => {
  const _id = req.body.buttons;
  try {
    const technician = await techModel.findById(_id);
    if (technician) {
      await techModel.findByIdAndDelete(_id);
      // Perform any other necessary database operations

      console.log('Deleted technician:', technician);
      res.redirect("/staff_details");
    } else {
      console.log("Technician not found.");
      res.redirect("/staff_details");
    }
  } catch (error) {
    console.error("Error deleting technician:", error);
    res.redirect("/staff_details");
  }
};


// exports.postTechnicianDetail = async (req, res) => {
//   const _id = req.body.button;
//   const technicians = await techModel.findById(_id);
//   if (technicians) {
//     await techModel.updateOne({ _id: _id }, { isDeleted: true });
//     res.redirect("/technician_details");
//   }
// };
exports.postTechnicianDetail = async (req, res) => {
  const _id = req.body.button;
  try {
    const technicians = await techModel.findById(_id);
    if (technicians) {
      await techModel.findByIdAndDelete(_id);
      // Perform any other necessary database operations

      console.log('Deleted technician:', technicians);
      res.redirect("/technician_details");
    } else {
      console.log("Technician not found.");
      res.redirect("/technician_details");
    }
  } catch (error) {
    console.error("Error deleting technician:", error);
    res.redirect("/technician_details");
  }
};

// exports.postTechnicianBulk = (req, res) => {
//   const userFile = req.files.userFile;

//   fs.createReadStream(userFile.path)
//     .pipe(csv())
//     .on('data', (row) => {
//       // Extract user information from each row
//       const name = row.Name;
//       const employeeId = row['Employee ID'];
//       const email = row.Email;
//       const contact = row['Contact No.'];
//       const password = row.Password;

//       // Perform registration logic with extracted data
//       // Register the user in your system
//     })
//     .on('end', () => {
//       // File processing is complete
//       // Optionally, you can redirect or send a response indicating the success of the bulk registration process.
//     });
// };
// app.delete("/technician_details/:id", (req, res) => {

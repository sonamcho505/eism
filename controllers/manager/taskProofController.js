const { transporter } = require("../../middlewares/transporter");
const Request = require("../../models/submitRequestModel");
const { default: axios } = require("axios");
const techModel = require("../../models/techModel");

exports.gettaskproof = async (req, res) => {
  try {
    const incidentRequests = await Request.find({}).sort({ _id: -1 });
    const currentUser = req.user.foundUser || req.user;
    console.log(incidentRequests);
    res.render("manager/taskproof", {
      data: currentUser,
      incidentRequests: incidentRequests,
    });
  } catch (err) {
    console.log(err);
  }
};

// exports.posttaskproof = async (req, res) => {
//   try {
//     const incidentRequests = await Request.find({}).sort({ _id: -1 });
//     const currentUser = req.user.foundUser || req.user;
//     const value = req.body.data;
//     const incidentRequest = await Request.findOne({ _id: value });
//     await incidentRequest.updateOne({ _id: value, status: "Completed" });
//     var mailOptions = {
//       from: "EMS",
//       to: incidentRequest.email,
//       subject: "Work Completion Notification",
//       html:
//         "Dear " +
//         incidentRequest.name +
//         ", <br><br>We are pleased to inform you that the request that you have sent to us has been completed. Here are the details of the completed request: <br><br> Request Type: " +
//         incidentRequest.requestType +
//         "<br>Request Date: " +
//         incidentRequest.requestDate +
//         "<br>Completion Date: " +
//         incidentRequest.completeDate +
//         "<br><br> If you have any questions,feedback, or require any further assitance, please feel free to reach put to us. We are always here to help and provide support. <br><br> Thank you for entrusting us with this task. We look forward to serving you again in the future. <br><br>Best regards, <br><br> Mr. Mindu Gyeltshen <br> 17594403 <br> Estate Incident and Service Management",
//     };

//     //sending mail
//     transporter.sendMail(mailOptions, function (error, info) {
//       if (error) {
//         console.log("email" + error);
//       } else {
//         res.render("manager/taskproof", {
//           data: currentUser,
//           incidentRequests: incidentRequests,
//         });
//       }
//     });
//     res.status(200).json({
//       success: true,
//       message: "Value received successfully",
//       value: value,
//     });
//   } catch (errr) {
//     console.log(err);
//   }
// };
exports.posttaskproof = async (req, res) => {
  try {
    const incidentRequests = await Request.find({}).sort({ _id: -1 });
    const currentUser = req.user.foundUser || req.user;
    const value = req.body.data;
    const incidentRequest = await Request.findOne({ _id: value });
    await incidentRequest.updateOne({ _id: value, status: "Completed" });
    var mailOptions = {
      from: "EMS",
      to: incidentRequest.email,
      subject: "Work Completion Notification",
      html:
        "Dear " +
        incidentRequest.name +
        ", <br><br>We are pleased to inform you that the request that you have sent to us has been completed. Here are the details of the completed request: <br><br> Request Type: " +
        incidentRequest.requestType +
        "<br>Request Date: " +
        incidentRequest.requestDate +
        "<br>Completion Date: " +
        incidentRequest.completeDate +
        "<br><br> If you have any questions, feedback, or require any further assistance, please feel free to reach out to us. We are always here to help and provide support. <br><br> Thank you for entrusting us with this task. We look forward to serving you again in the future. <br><br>Best regards, <br><br> Mr. Mindu Gyeltshen <br> 17594403 <br> Estate Incident and Service Management",
    };

    // Sending mail
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log("email" + error);
      } else {
        res.render("manager/taskproof", {
          data: currentUser,
          incidentRequests: incidentRequests,
        });
      }
    });
  } catch (error) {
    console.log(error);
  }
};


// exports.postRejecttaskproof = async (req, res) => {
//   try {
//     const { requestId, rejectionComment } = req.body;
//     const incidentRequest = await Request.findOne({ _id: requestId });
//     await incidentRequest.updateOne({ _id: requestId }, { status: "Rejected", remarks: rejectionComment });
//     res.status(200).json({
//       success: true,
//       message: "Value received successfully",
//     });
//   } catch (err) {
//     console.log(err);
//   }
// };

// Import the necessary dependencies

// Handle the POST request to reject a task proof
// exports.postRejecttaskproof = async (req, res) => {
//   try {
//     const requestId = req.body.requestId;
//     const rejectionComment = req.body.rejectionComment;

//     // Find the incident request by ID
//     const incidentRequest = await Request.findOne({ _id: requestId });

//     if (!incidentRequest) {
//       return res.status(404).json({ success: false, message: 'Incident request not found' });
//     }

//     // Update the incident request status to "Rejected" and add the rejection comment
//     incidentRequest.status = 'Rejected';
//     incidentRequest.rejectionComment = rejectionComment;

//     // Save the updated incident request
//     await incidentRequest.save();

//     res.status(200).json({
//       success: true,
//       message: 'Incident request rejected successfully',
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ success: false, message: 'Internal server error' });
//   }
// };

exports.postRejecttaskproof = async (req, res) => {
  try {
    const requestId = req.body.requestId;
    const rejectionComment = req.body.rejectionComment;
    console.log(rejectionComment);
    await Request.updateOne(
      { _id: requestId },
      {
        $set: {
          status: "Rejected",
          progress_status: "pending",
          remarks: rejectionComment,
        },
      }
    );
    const incidentRequest = await Request.findOne({ _id: req.body.requestId });
    const technicianName = incidentRequest.assignedTechnician.split("-")[0];
    const findTechnicianEmail = await techModel.find({ name: technicianName });
    const [found] = findTechnicianEmail;
    console.log(incidentRequest.email);
    var mailOptions = {
      from: "EMS",
      to: found.email,
      subject: "Work Rejection Notification",
      html:
        "Dear " +
        technicianName +
        ", <br><br>I hope this message finds you well. I am writing to inform you that after careful consideration and evaluation, we have made the decision to reject your submitted work. We appreciate the time and effort you invested in completing the task, and we understand that this news may be disappointing. Here is the reasons for our decision: <br><br> " +
        incidentRequest.remarks +
        "<br><br> Thank you once again for your effort and for considering us for your work. We appreciate your understanding. <br><br> Kind regards, <br><br> Estate Manager <br> Estate Incident and Service Management",
    };

    //sending mail
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log("email" + error);
      } else {
        res.status(200).json({
          success: true,
          message: "Incident request rejected successfully",
        });
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
};

// exports.postRejecttaskproof = async (req, res) => {
//   try {
//     const requestId = req.body.requestId;
//     const rejectionComment = req.body.rejectionComment;
//     console.log(rejectionComment);
//     const value = req.body.data;
//     const incidentRequest = await Request.findOne({ _id: value });
//     console.log(incidentRequest);
//     await Request.updateOne({ _id: requestId }, {
//       $set: { status: "Rejected", progress_status: "pending", remarks: rejectionComment }
//     });

//     res.status(200).json({
//       success: true,
//       message: 'Incident request rejected successfully',
//     });

//     const technicianName = incidentRequest.assign.split("-")[0];
//     const findTechnicianEmail = await techModel.find({ name: technicianName });
//     const [found] = findTechnicianEmail;

//     if (found) {
//       const recipients = [
//         {
//           email: found.email,
//           message: `Dear ${technicianName},
//           <br><br>We hope this message finds you well. We would like to inform you that a task that has been assigned to you has been reject for the below user. Please review the details below:
//           <br><br>Name: ${incidentRequest.name}
//           <br>Request Type: ${incidentRequest.requestType}
//           <br>Description: ${incidentRequest.description}.
//           <br><br>Try to redo the work and submit the work evident again, please log in to our system using your credentials. If you encounter any issues or have any questions regarding the task, please reach out to the manager.
//           <br><br>We appreciate your dedication and commitment to your responsibilities. Your contribution plays a vital role in our collective success. Thank you for your cooperation.
//           <br><br>Best regards,
//           <br>Mr. Mindu Gyeltshen
//           <br>17594403
//           <br>Estate Incident and Service Management`
//         }
//       ];

//       recipients.forEach(recipient => {
//         const mailOptions = {
//           from: "EMS",
//           to: recipient.email,
//           subject: "Work Rejected Notification",
//           html: recipient.message
//         };

//         // Sending mail
//         transporter.sendMail(mailOptions, function (error, info) {
//           if (error) {
//             console.log("email" + error);
//           }
//         });
//       });
//     }

//     res.render("manager/taskproof", {
//       data: currentUser,
//       incidentRequests: incidentRequests,
//     });

//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ success: false, message: 'Internal server error' });
//   }
// };

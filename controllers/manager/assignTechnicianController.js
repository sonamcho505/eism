const Request = require("../../models/submitRequestModel");
const { default: axios } = require("axios");
const { transporter } = require("../../middlewares/transporter");
const techModel = require("../../models/techModel");

exports.getAssignTechnician = async (req, res) => {
  const technicianUsers = await techModel.find({});
  const incidentRequests = await Request.find({ _id: req.params.id });
  const currentUser = req.user.foundUser || req.user;
  res.render("manager/Assign_technician", {
    data: currentUser,
    incidentRequests: incidentRequests,
    assigns: technicianUsers,
  });
};

// exports.postAssignTechnician = async (req, res) => {
//   const assign = req.body.assign;
//   const incidentRequest = await Request.findOne({ _id: req.params.id });
//   await incidentRequest.updateOne({
//     _id: req.params.id,
//     assignedTechnician: assign,
//   });

//   const technicianName = assign.split("-")[0];
//   const findTechnicianEmail = await techModel.find({ name: technicianName });
//   const [found] = findTechnicianEmail;
//   console.log(incidentRequest.email);
  
//   if (findTechnicianEmail) {
//     var recipients = [
//       { email: incidentRequest.email, message: "Your Requested has been assigned to a particular technician." },
//       { email: found.email, message: "You have been assigned with the request!" }
//     ];
//     console.log(recipients)
//   recipients.map(e=>{
//     console.log(e.email)
//     var mailOptions = recipients.map(recipient => ({
//           from: "EState Management",
//           to: e.email,
//           subject: "New Message",
//           html: recipient.message
//         }));
//         transporter.sendMail(mailOptions, function (error, info) {
//                 if (error) {
//                   console.log(error);
//                 } else {
//                   console.log("Assigned"+ info);
//                   res.redirect("/managerDashboard");
//                 }
//               });
//   })
//     // if (recipients.length > 0) {
//     //   var mailOptions = recipients.map(recipient => ({
//     //     from: "EState Management",
//     //     to: recipient.email,
//     //     subject: "New Message",
//     //     html: recipient.message
//     //   }));
  
//   //     transporter.sendMail(mailOptions, function (error, info) {
//   //       if (error) {
//   //         console.log(error);
//   //       } else {
//   //         console.log("Assigned");
//   //         res.redirect("/managerDashboard");
//   //       }
//   //     });
//   //   } else {
//   //     console.log("No recipients defined");
//   //   }
  
//   // }else{
//     console.log("Not found email!");
//     res.redirect("/managerDashboard");
//   }
// };
exports.postAssignTechnician = async (req, res) => {
  const assign = req.body.assign;
  const incidentRequest = await Request.findOne({ _id: req.params.id });
  await incidentRequest.updateOne({
    _id: req.params.id,
    assignedTechnician: assign,
  });

  const technicianName = assign.split("-")[0];
  const findTechnicianEmail = await techModel.find({ name: technicianName });
  const [found] = findTechnicianEmail;
  console.log(incidentRequest.email);

  if (found) {
    var recipients = [
      { email: incidentRequest.email, message: "Dear "+incidentRequest.name+
      ",<br><br>We hope this message finds you well. We would like to inform you that your request that has been sent on " +incidentRequest.requestDate+ " has been assigned to " + technicianName+
      ". To check the progres of your request please log into the system using your credentials. Thank you for your cooperation. <br><br> Best regards, <br><br>Mr. Mindu Gyeltshen  <br> 17594403 <br> Estate Incident and Service Management"},
      { email: found.email, message: "Dear "+technicianName+
      ",<br><br>We hope this message finds you well. We would like to inform you that a new task has been assigned to you in our system. Please review the details below: <br><br> Name: " + incidentRequest.name+ "<br> Request Type:  "+incidentRequest.requestType+ "<br> Description: " +incidentRequest.description+ ". <br><br>To access the task and begin working on it, please log in to our system using your credentials. If you encounter any issues or have any questions regarding the task, please reach out to the manager. <br><br> We appreciate your dedication and commitment to your responsibilities. Your contribution plays a vital role in our collective success. Thank you for your cooperation. <br><br> Best regards, <br><br> Mr. Mindu Gyeltshen <br> 17594403 <br> Estate Incident and Service Management"}
    ];

    recipients.forEach(recipient => {
      var mailOptions = {
        from: "EState Management",
        to: recipient.email,
        subject: "Assigned Request Notification",
        html: recipient.message
      };

      transporter.sendMail(mailOptions, function (error) {
        if (error) {
          console.log(error);
        } else {
          console.log("Assigned");
        }
      });
    });

    res.redirect("/managerDashboard");
  } else {
    console.log("Technician email not found!");
    res.redirect("/managerDashboard");
  }
};

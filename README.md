# Project Title
Estate Management System: Service Request Tracking and Management System 

# Problem Statement
The Gyalpozhing College of Information Technology's staff and students currently use a Google Form to report the incidents whenever there are problems with the properties. The maintenance team is then notified via mail and must manually check the details from Google Sheets (M.Gyeltshen, personal communication, March 7, 2023). The individuals making the request are also unaware of whether the maintenance team has received the request, when the request will be resolved, or if the request has been resolved at all. With many incidents being reported, the maintenance team are having trouble with keeping the record as they have to manually enter all the details in the excel sheet which are prone to errors. 

Therefore, with this web application, the users will be able to request the services, track the status of those requests, and be notified when a problem is resolved. On the other hand, the maintenance team will be able to keep a record and also to track the services that are being requested.

# Aim
The aim of the project is to develop a web based application for estate incident and service management.

# Technology
1. VS Code
2. GitLab
3. Express/ Node JS
4. MongoDB
5. Bootstrap
6. Asana: Allows teams to create and assign tasks, set due dates, and track progress. This helps to ensure that everyone is working on the right tasks and that deadlines are met.

# Project Guide
Tshering (Assitent Professor)

# Team Members and Role
1. Soname Choki (78) (Scrum Master)
2. Ngawang Choden (Developer)
3. Pema Zangmo (Developer)
4. Sonam Choki (Developer)
5. Sonam Dendup (Developer)


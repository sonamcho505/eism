const express = require("express");
const router = express.Router();
const multer = require("multer");

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     // Specify the destination folder for uploaded files
//     cb(null, '../../public/upload/');
//   },
//   filename: function (req, file, cb) {
//     // Generate a unique filename for the uploaded file
//     cb(null, Date.now() + '-' + file.originalname);
//   }
// });

// const upload = multer({ storage: storage });
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/uploads");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString + file.originalname);
  },
});

var upload = multer({
  storage: storage,
});

const addTechnicianUsers = require("../controllers/manager/addTechnicianController");
const { currentUser } = require("../middlewares/currentUser");

router.get("/add_technician", currentUser, addTechnicianUsers.getTechUser);
router.post("/add_technician", addTechnicianUsers.postTechUser);

router.get(
  "/technician_update_form/:id",
  currentUser,
  addTechnicianUsers.getTechnicianUpdateForm
);
router.post(
  "/technician_update_form/:id",
  currentUser,
  addTechnicianUsers.postTechnicianUpdateForm
);

router.get(
  "/staff_update_form/:id",
  currentUser,
  addTechnicianUsers.getUserUpdateForm
);
router.post(
  "/staff_update_form/:id",
  currentUser,
  addTechnicianUsers.postUserUpdateForm
);

router.get("/add_user", currentUser, addTechnicianUsers.getUser);
router.post("/add_user", addTechnicianUsers.postUser);

router.post("/bulk-registration",upload.single("csvFile"),addTechnicianUsers.postTechnicianBulkRegister);
router.get("/download-csv-template", addTechnicianUsers.getTechnicianBulkRegister);

router.post("/bulk-registrations",upload.single("csvFile"),addTechnicianUsers.postUserBulkRegister);
router.get("/download-csv-templates", addTechnicianUsers.getUserBulkRegister);
module.exports = router;

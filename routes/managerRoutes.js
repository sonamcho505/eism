const express = require("express");
const router = express.Router();
const Request = require("../models/submitRequestModel")


const managerUser = require("../controllers/manager/managerController");
const { currentUser } =  require("../middlewares/currentUser");
const { User } =  require("../middlewares/currentUser");

router.get("/managerDashboard",currentUser, managerUser.getManagerDashboard);
router.post("/managerDashboard", managerUser.postManagerDashboard);
router.get("/viewUserRequest/:id",currentUser, managerUser.getUserViewRequest);

router.get("/switchToUser1", currentUser, managerUser.getSwitchManager);

module.exports = router;
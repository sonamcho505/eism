const express = require("express");
const router = express.Router();
const multer = require("multer");


const technician_form = require("../controllers/technician/technicianFormController");
const { User } =  require("../middlewares/currentUser");
// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/uploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

var upload = multer({
  storage: storage,
});

router.get("/technician_form/:id", User, technician_form.getTechnicianForm);
router.post("/technician_form/:id",User, upload.single("image"),technician_form.postTechnicianForm);


router.get("/viewAssignRequest/:id",User, technician_form.getViewAssignRequest)
module.exports = router;

const express = require("express");
const router = express.Router();

const techUser = require("../controllers/technician/techUserController");
const { User } =  require("../middlewares/currentUser");

router.get("/technicianDashboard", User, techUser.getTechnicianDashboard);
router.post("/technicianDashboard", User, techUser.postTechnicianDashboard);
router.get("/switchToUser", User, techUser.getSwitchTechnician);


module.exports = router;


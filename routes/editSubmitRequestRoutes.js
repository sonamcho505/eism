const express = require("express");
const router = express.Router();

const editSubmitRequest = require("../controllers/users/editSubmitRequestController");
const { User } =  require("../middlewares/currentUser");


router.get("/editSubmitRequest/:id",User, editSubmitRequest.geteditSubmitRequest);
router.post("/editSubmitRequest/:id",User, editSubmitRequest.posteditSubmitRequest);
router.get("/viewRequest/:id",User, editSubmitRequest.getviewRequest);


module.exports = router;
const express = require("express");
const router = express.Router();

const technician_details = require("../controllers/manager/technicianDetailsController");
const { currentUser } = require("../middlewares/currentUser");


router.get("/technician_details",currentUser, technician_details.getTechnicianDetail);
router.post("/technician_details",currentUser, technician_details.postTechnicianDetail);

router.get("/staff_details",currentUser, technician_details.getUserDetail);
// router.post("/staff_details/:id",currentUser, technician_details.postUserDetail);
router.post("/staff_details",currentUser, technician_details.postUserDetail);
// router.post("/bulk_registration",currentUser, technician_details.postTechnicianBulk);


module.exports = router;


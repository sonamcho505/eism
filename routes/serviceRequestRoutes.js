const express = require("express");
const router = express.Router();

const service_request = require("../controllers/manager/serviceRequestController");
const { currentUser } =  require("../middlewares/currentUser");


router.get("/service_request",currentUser, service_request.getServiceRequest);

module.exports = router;


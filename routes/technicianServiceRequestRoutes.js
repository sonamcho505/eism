const express = require("express");
const router = express.Router();

const technician_service_request = require("../controllers/technician/technicianServiceRequestController");
const { User } =  require("../middlewares/currentUser");


router.get("/technician_service_request",User, technician_service_request.getTechnicianServiceRequest);

module.exports = router;


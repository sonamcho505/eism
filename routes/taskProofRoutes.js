const express = require("express");
const router = express.Router();

const taskproof = require("../controllers/manager/taskProofController");
const { currentUser } =  require("../middlewares/currentUser");


router.get("/taskproof",currentUser, taskproof.gettaskproof);
router.post("/taskproof",currentUser, taskproof.posttaskproof);
router.post("/Rejecttaskproof",currentUser, taskproof.postRejecttaskproof);

module.exports = router;


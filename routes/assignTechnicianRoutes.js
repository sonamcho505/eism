const express = require("express");
const router = express.Router();

const Assign_Technician = require("../controllers/manager/assignTechnicianController");
const { currentUser } = require("../middlewares/currentUser");


router.get("/Assign_Technician/:id",currentUser, Assign_Technician.getAssignTechnician);
router.post("/Assign_Technician/:id",currentUser, Assign_Technician.postAssignTechnician);

module.exports = router;


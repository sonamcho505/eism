const express = require("express");
const router = express.Router();

const technician_incident_request = require("../controllers/technician/technicianIncidentRequestController");
const { User } =  require("../middlewares/currentUser");

router.get("/technician_incident_request",User, technician_incident_request.getTechnicianIncidentRequest);
router.post("/technician_incident_request", technician_incident_request.postTechnicianIncidentRequest)

module.exports = router;


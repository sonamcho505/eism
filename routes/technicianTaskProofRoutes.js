const express = require("express");
const router = express.Router();

const techniciantaskproof = require("../controllers/technician/technicianTaskProofController");
const { User } =  require("../middlewares/currentUser");
router.get("/techniciantaskproof",User, techniciantaskproof.gettechniciantaskproof);
router.post("/techniciantaskproof",User, techniciantaskproof.posttechniciantaskproof);
// router.post("/techniciantaskproof",User, techniciantaskproof.postDeleteTechnicianRequest);

module.exports = router;


const express = require("express");
const router = express.Router();

const work_report = require("../controllers/manager/workReportController");
const { currentUser } =  require("../middlewares/currentUser");


router.get("/work_report",currentUser, work_report.getWorkReport);
router.get("/download",currentUser, work_report.getDownloadCSVFile);

module.exports = router;
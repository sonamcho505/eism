const express = require("express");
const router = express.Router();

const workstatus = require("../controllers/users/workStatusController");
const { User } =  require("../middlewares/currentUser");


router.get("/workstatus",User, workstatus.getWorkStatus);
// router.post("/workstatus",User, workstatus.postWorkStatus);
module.exports = router;
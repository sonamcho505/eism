const express = require("express");
const router = express.Router();

const submit_request = require("../controllers/users/submitRequestController");
const { User } =  require("../middlewares/currentUser");
// router.set('view engine', 'ejs');
// router.set('views', './views/users');

router.get("/submitrequest",User, submit_request.getSubmitRequest);
router.post("/submitrequest", submit_request.postSubmitRequest);

module.exports = router;


const express = require("express");
const router = express.Router();

const incident_request = require("../controllers/manager/incidentController");
const { currentUser } =  require("../middlewares/currentUser");


router.get("/incident_request",currentUser, incident_request.getIncidentRequest);
router.post("/incident_request", incident_request.postIncidentRequest)

module.exports = router;


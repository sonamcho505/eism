const express = require("express");
const {
  viewUserDashboard,
  loadLogin,
  postLogin,
  getLogout,
  postUserLogin,
  postDelete,

  getForgotPassword,
  postForgotPassword,
  getResetPassword,
  postResetPassword,
  getForgotPasswordSuccess,
  getResetPasswordSuccess
} = require("../controllers/users/userController");
const { User } =  require("../middlewares/currentUser");
const router = express.Router();

router.get("/", loadLogin);
router.post("/", postUserLogin);
router.get("/userdashboard", User, viewUserDashboard);
router.get("/logout", getLogout);
router.post("/userdashboard",User, postDelete);

// user_route.get('/forget', userController.forgetLoad);
// user_route.post('/forget', userController.forgetVerify);

// user_route.get('/reset-password',userController.resetPasswordLoad);
// user_route.post('/reset-password', userController.resetPassword);

// user_route.get('/otp', userController.loadOTP);
// user_route.post('/otp', userController.enterOTP);
router.get('/forgot-password', getForgotPassword);
router.post('/forgot-password', postForgotPassword);

router.get('/reset-password', getResetPassword);
router.post('/reset-password', postResetPassword);
router.get('/forgot-password-success', getForgotPasswordSuccess);
router.get('/reset-password-success', getResetPasswordSuccess);

module.exports = router;

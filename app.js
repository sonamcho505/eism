require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
// const authRouter = require("./routes/auth");
const session = require("express-session");
const flash = require('connect-flash');
const app = express();

const userRoute = require("./routes/userRoutes");
const techRoute = require("./routes/techUserRoutes");
const managerRoute = require("./routes/managerRoutes");
const addTechnicianUsers = require("./routes/addTechnicianRoutes");
const incidentRequestRoute = require("./routes/incidentRequestRoutes");
const serviceRequestRoute = require("./routes/serviceRequestRoutes");
const taskproof = require("./routes/taskProofRoutes");
const techniciandetailsRoute = require("./routes/technicianDetailRoutes");
const workReportRoute = require("./routes/workReportRoutes");
const assignTechnicianRoute = require("./routes/assignTechnicianRoutes");
const submitRequestRoute = require("./routes/submitRequestRoutes");
const editSubmitRequestRoute = require("./routes/editSubmitRequestRoutes");
const workStatusRoute = require("./routes/workStatusRoutes");
const technicianFormRoute = require("./routes/technicianFormRoutes");
const technicianIncidentRequestRoute = require("./routes/technicianIncidentRequestRoutes");
const technicianServiceRequestRoute = require("./routes/technicianServiceRequestRoutes");
const technicianTaskProofRoute = require("./routes/technicianTaskProofRoutes");

const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");
app.use(express.static(path.join(__dirname, "views")));
app.use(bodyParser.json());
app.use(express.json());

app.use(express.urlencoded({ extended: true })); //
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(session({
  secret: 'your-secret-key',
  resave: false,
  saveUninitialized: false
}));
app.use(flash());

mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connected to mongo.");
  })
  .catch((err) => {
    console.log("Error connecting to mongo.", err);
  });
app.use(cookieParser());

// app.use(session({
//   secret: 'your-secret-key', // replace with your own secret key
//   resave: false,
//   saveUninitialized: true,
// }));

// AUTH CONFIG
// app.use(
//   session({
//       secret: process.env.SECRET,
//       resave: false,
//       saveUninitialized: false,
//       store: new mongoStore({ mongooseConnection: mongoose.connection }),
//       cookie: { maxAge: 86400000 },
//   }),
// );

// app.use(session({
//   secret: process.env.SECRET,
//   resave: false,
//   saveUninitialized: true,
// }));

// Add other routes and middleware here

// app.use("/",authRouter);
app.use(userRoute);
app.use("/", techRoute);
app.use("/", managerRoute);
app.use("/", addTechnicianUsers);
app.use("/", incidentRequestRoute);
app.use("/", serviceRequestRoute);
app.use("/", taskproof);
app.use("/", techniciandetailsRoute);
app.use("/", workReportRoute);
app.use("/", assignTechnicianRoute);
app.use("/", submitRequestRoute);
app.use("/", workStatusRoute);
app.use("/", technicianFormRoute);
app.use("/", editSubmitRequestRoute);
app.use("/", technicianIncidentRequestRoute);
app.use("/", technicianServiceRequestRoute);
app.use("/", technicianTaskProofRoute);

const port = 8000;
app.listen(port, () => {
  console.log(`App is running on port ${port}...`);
});

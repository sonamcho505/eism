const mongoose = require("mongoose");

const techSchema = new mongoose.Schema({
  employee_id: {
    type: String,
    require: true,
  },
  name: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
  contact: {
    type: String,
    require: true,
  },
  designation: {
    type: String,
    default: "",
  },
  role: {
    type: String,
    require: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  resetToken: {
    type: String,
    default: '',
  },
  profile: {
    type: String,
  },
  // is_varified: {
  //   type: Number,
  //   default: 0,
  // },
});

const techModel = mongoose.model("TechModel", techSchema);
module.exports = techModel;

const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define schema
const requestSchema = new Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  contactNo: {
    type: String,
  },
  memberType: {
    type: String,
  },
  block: {
    type: String,
  },
  roomNo: {
    type: String,
  },
  houseNo:{
    type: String,
  },
  requestType: {
    type: String,
  },
  requestDate: {
    type: String,
  },
  requestTime: {
    type: String,
  },
  description: {
    type: String,
  },
  assignedTechnician: {
    type: String,
  },
  status: {
    type: String,
  },
  progress_status: {
    type: String,
  },
  completeDate: {
    type: String,
    default: ""
  },
  completeTime: {
    type: String,
  },
  itemList: {
    type: String,
  },
  quantity: {
    type: String,
  },
  imageUrl: {
    type: String,
  },
  isReject:{
    type: String,
  },
  soft_delete: {
    type: Boolean,
    default: false
  },
  _delete: {
    type: Boolean,
    default: false
  },
  remarks:{
    type: String,
  }
});

// Create model
const Request = mongoose.model('Request', requestSchema);

module.exports = Request;

